const std = @import("std");

const toto : Function = fn () void {
    std.debug.print("toto", .{});
}

pub fn main() void {
    toto();
}