const std = @import("std");



pub fn main() void {

    const tab = [3]u3 {1, 2, 3};
    const chars = [_]u8 {'H', 'e', 'l', 'l', 'o'};

    
    std.debug.print("tab[2] = {d}\n", .{tab[2]});

    var i : usize = 0;
    while (i < chars.len) : (i+= 1) {
        std.debug.print("{c}", .{chars[i]});
    }

    std.debug.print("\n", .{});

}