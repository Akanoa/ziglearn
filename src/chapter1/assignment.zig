const std = @import("std");

const constant : i32 = 12;
var variable : u4 = undefined;
const inferred = @as(u7, 12);


pub fn main() void {
    variable = 4;
    std.debug.print("v = {d}\n", .{variable});
    std.debug.print("i = {d}\n", .{inferred});
}